package org.pilar.prueba;


import javax.annotation.Resource;
import javax.jws.WebService;
import javax.xml.bind.DatatypeConverter;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

@WebService
public class BuildingInfo {
    @Resource
    WebServiceContext context;


    public String getInfo() throws IOException {
        if (isAuthenticated()) {
            JSONObject item = new JSONObject();
            item.put("property", "technopark");
            item.put("type", "business");
            item.put("numOfPeople", 500);
            StringWriter out = new StringWriter();
            item.writeJSONString(out);

            String jsonText = out.toString();
            return jsonText;
        } else {
            return "Invalid user name or password";
        }


    }

    public String validPerson(Person p) {
        if (isAuthenticated()) {
            if (p.name!=null && p.age!=null){
                if (p.age>65){
                    return p.name + " should stay home";
                }
                return  p.name + " can go to work";
            }
            else{
                return "Name or age missing";
            }

        } else {
            return "Invalid user name or password";
        }

    }


    private Boolean isAuthenticated() {

        MessageContext messageContext = context.getMessageContext();

        Map<String, List<String>> headers;
        headers = (Map<String, List<String>>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);

        //The header "Basic base64(user:password)
        String authHeader = headers.get("Authorization").get(0);

        //Remove "Basic "
        String authtoken = authHeader.split(" ")[1];

        //Decode base64 and read username and password
        String token = new String(DatatypeConverter.parseBase64Binary(authtoken));
        String tokenS[] = token.split(":");
        String username = tokenS[0];
        String password = tokenS[1];
        switch (username) {
            case "admin":
                return password.equals("admin");
            case "pilar":
                return password.equals("asdf");
            default:
                return false;
        }
    }
}

